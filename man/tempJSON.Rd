% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{tempJSON}
\alias{tempJSON}
\title{Get template for a new resource instance}
\usage{
tempJSON(resource = c("Antibody", "Assay", "Proteomics"))
}
\arguments{
\item{resource}{Name of resource type.}
}
\description{
Get template for a new resource instance
}
