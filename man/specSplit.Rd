% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{specSplit}
\alias{specSplit}
\title{Split fields to pre-determined specification}
\usage{
specSplit(field)
}
\arguments{
\item{field}{One-character vector representing a field.}
}
\description{
Fields that contain multiple values MUST use pipe ("|") separator,
which this utility function assumes for routine parsing.
It is used within all `*_json` functions that convert resource records to JSON.
}
