% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/mappers.R
\name{map_Application}
\alias{map_Application}
\title{Map to a resource application object}
\usage{
map_Application(Publication, Usage, UsageNotes, Rating, Contacts)
}
\arguments{
\item{Publication}{Publication ID.}

\item{Usage}{Usage type.}

\item{UsageNotes}{Usage notes text.}

\item{Rating}{Integer rating.}

\item{Contacts}{Person contacts.}
}
\description{
The object ResourceApplication is a JSON containing
Contacts, Publication, Usage, UsageNotes, Rating
}
