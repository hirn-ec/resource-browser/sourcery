% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/p_utils.R
\name{CellTissue}
\alias{CellTissue}
\title{CellTissue}
\usage{
CellTissue(record)
}
\arguments{
\item{record}{A JSON record of a single resource.}
}
\description{
CellTissue
}
\seealso{
Other biosampletype_attr: 
\code{\link{CellLine}()},
\code{\link{Species}()}
}
\concept{biosampletype_attr}
