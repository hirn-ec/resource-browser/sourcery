% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{formatCol}
\alias{formatCol}
\title{Format a list column in exported .csv file}
\usage{
formatCol(col)
}
\arguments{
\item{col}{A list (multiple-select) column with entries separated by ",".}
}
\description{
Note that this is tested specifically for Airtable-exported files.
}
