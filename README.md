## R package to interact with HIRN Resource Browser

This is an R package providing functionality and examples for using the catalog of resources at the HIRN Resource Browser via the REST API documented at https://resourcebrowser.hirnetwork.org/Data/Index. It is available for use by all researchers who wish to access the data in the Resource Browser programmatically. 

### Installation

Since it is still in development, you need the `devtools` package to install using `devtools::install_gitlab("HIRN-EC/resource-browser/Sourcery")`. 

### Advanced

Most users may start using the package to retrieve resources, but there is also functionality to submit resources through an Extract, Transform, Load (ETL) pipeline. We plan to describe this portion in greater detail to aid submission of resources programmatically.

