# Functions that interact with the HIRN Resource Browser API r_ endpoints

#' Get resource schema
#'
#' Type is a recognized resource type (Antibody, CellLine), otherwise returns schema for all types
#' @param apikey Api key.
#' @export
getResourceLoadSchema <- function(apikey = getOption("apikey")) {
  httr::GET("https://resourcebrowser.hirnetwork.org/api/v1/r_ResourceLoad/GetSchema",
            httr::add_headers(apikey = apikey, Accept = "application/json",
                        `Content-Type` = "application/json"))
}

#' Get schema for Application document object
#'
#' @inheritParams getResourceLoadSchema
#' @export
getAppLoadSchema <- function(apikey = getOption("apikey")) {
  response <- httr::GET("https://resourcebrowser.hirnetwork.org/api/v1/ApplicationLoad/GetSchema",
                        httr::add_headers(apikey = apikey, Accept = "application/json",
                                    `Content-Type` = "application/json"))
}

#' Get sample JSON for Application Object
#'
#' @inheritParams getResourceLoadSchema
#' @export
getSampleLoadJson <- function(apikey = getOption("apikey")) {
  response <- httr::GET("https://resourcebrowser.hirnetwork.org/api/v1/r_ResourceLoad/GetSampleJson",
                        httr::add_headers(apikey = apikey, Accept = "application/json",
                                    `Content-Type` = "application/json"))
  status <- httr::status_code(response)
  # response <- httr::content(response)
  response
}


#' Get CodedValues
#'
#' @inheritParams getResourceLoadSchema
#' @export
getCodedValues <- function(apikey = getOption("apikey")) {
  response <- httr::GET("https://resourcebrowser.hirnetwork.org/api/v1/r_ResourceLoad/GetCodedValues",
                        httr::add_headers(apikey = apikey, Accept = "application/json",
                                    `Content-Type` = "application/json"))
  status <- httr::status_code(response)
  # response <- content(response)
  response
}


#' Validate a resource load object
#'
#' @param body JSON resource.
#' @param endpoint Either "dev" or "test" endpoint.
#' @inheritParams getResourceLoadSchema
#' @export
resourceLoadValidate <- function(apikey = getOption("apikey"), body, endpoint = c("dev", "prod", "test")) {
  if(endpoint == "prod") {
    endpoint <- "https://resourcebrowser.hirnetwork.org/api/v1/r_ResourceLoad/Validate"
  } else if(endpoint == "dev") {
    endpoint <- "https://dev-resourcebrowser.azurewebsites.net/api/v1/r_ResourceLoad/Validate"
  } else if(endpoint == "test") {
    endpoint <- "https://test-resourcebrowser.azurewebsites.net/api/v1/r_ResourceLoad/Validate"
  }
  httr::POST(endpoint,
             httr::add_headers(apikey = apikey, Accept = "application/json",
                         `Content-Type` = "application/json"),
             body = body)
}

#' Load resources
#'
#' @inheritParams  resourceLoadValidate
#' @param body JSON resource.
#' @export
loadResource <- function(apikey = getOption("apikey"), body) {
  httr::POST("https://resourcebrowser.hirnetwork.org/api/v1/r_ResourceLoad/Load",
             httr::add_headers(apikey = apikey, Accept = "application/json",
                         `Content-Type` = "application/json"),
             body = body)
}





# Application --------------------------------------------------------------------------------------------------#

#' Validate an application
#'
#' @inheritParams  resourceLoadValidate
#' @export
validateApplication <- function(apikey = getOption("apikey"), body = NULL) {
  httr::POST("https://test-resourcebrowser.azurewebsites.net/api/v1/ApplicationLoad/Validate",
             httr::add_headers(apikey = apikey, Accept = "application/json",
                         `Content-Type` = "application/json"),
             body = body)
}

#' Load an application
#'
#' @inheritParams  resourceLoadValidate
#' @export
loadApplication <- function(apikey = getOption("apikey"), body = NULL) {
  httr::POST("https://resourcebrowser.hirnetwork.org/api/v1/ApplicationLoad/Load",
             httr::add_headers(apikey = apikey, Accept = "application/json",
                         `Content-Type` = "application/json"),
             body = body)
}


# Contributors --------------------------------------------------------------------------------------------------#
#' Retrieve all contributors
#'
#' @param apikey The api key required for using this endpoint.
#' @export
getContributorsList <- function(apikey = getOption("apikey")) {
  response <- httr::GET(url = paste0("https://resourcebrowser.hirnetwork.org/api/v2/p_Contributor/List"),
                        httr::add_headers(apikey = apikey, Accept = "application/json"))
  status <- httr::status_code(response)
  if(status != 200L) stop(sprintf("API request failed [%s]", httr::status_code(response)))
  response <- httr::content(response)
}
