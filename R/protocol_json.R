# -- PROTOCOL -------------------------------------------------------------------------------------------------------------#

#' Convert protocol records to JSON
#'
#' @inheritParams ds_json
#'
#' @export
protocol_json <- function(x,
                          subtype = NULL,
                          newJSON = tempJSON("Protocol"),
                          typeJSON,
                          batch,
                          toJSON = FALSE) {
  
  new <- newJSON

  new$PrimaryResourceType <- Sourcery::resourceTypes$ResourceTypes[[3]][Sourcery::resourceSchema]
  new$PrimaryResourceType$SerializationMode <- "full"
  
  new$SecondaryResourceType <- typeJSON[[1]] # revise if Protocol gets split into subtypes
  new$SecondaryResourceType$SerializationMode <- "full"
  
  new$CurationStatus <- Sourcery::codedValues$CodedValuesByType$CurationStatus[[3]]
  new$CurationStatus$SerializationMode <- "full"
  
  new$Name <- x$Name
  new$Description <- x$Description
  new$Contributors <- lapply(specSplit(x$Contributors), map_Contributor) # unlike w/ other resources, don't use map_ContributorsDefault
  new$DefiningManuscriptId <- x$PMID
  new$CanonicalId <- x$CanonicalId
  new$CanonicalUrl <- x$URL
  new$Id <- uuid::UUIDgenerate()
  new$BatchIdentifier <- batch
  
  new$Vendor <- newVendor()
  
  if(toJSON) jsonlite::toJSON(new, pretty = T, null = "null", na = "null", auto_unbox = T) else new
}


#' Wrapper for converting many protocol records to JSON
#'
#' @inheritParams ds_json_batch
#'
#' @export
protocol_json_batch <- function(srctable = NULL,
                                file,
                                forenamechars = 3,
                                batch,
                                toJSON = FALSE) {
  
  if(is.null(srctable)) srctable <- data.table::fread(file, colClasses = "character", na.strings = NULL)
  # get latest templates
  newJSON <- jsonlite::fromJSON("http://test-resourcebrowser.azurewebsites.net/api/v1.0/AdminResource/GetNew/Protocol")
  
  typeJSON <- lapply(Sourcery::resourceTypes$ResourceTypes[[3]]$SubResourceTypes, function(x) x[Sourcery::resourceSchema])
  resourcelist <- apply(srctable, 1, as.list)
  resourcelist <- lapply(resourcelist,
                         function(x) try(protocol_json(x, newJSON = newJSON, typeJSON = typeJSON, batch = batch, toJSON = toJSON)))
  
  return(resourcelist)
}
